/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6zad2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author Stud
 */
public class Node 
{
    private int nodeNumber;
    private int myPort;
    
    public Node(int nodeNumber, int myPort)
    {
        this.nodeNumber = nodeNumber;
        this.myPort = myPort;
    }
    
    public void server()
    {
        try(ServerSocket server = new ServerSocket(myPort);)
        {
            System.out.println("uruchomiono serwer");
            while(true)
            {
                try(Socket client = server.accept();
                    InputStream in = client.getInputStream();
                    InputStreamReader reader = new InputStreamReader(in,"ASCII");)
                {
                    System.out.println("Klient polaczony");
                    StringBuilder sb = new StringBuilder();
                    for (int c = reader.read(); c != -1;c=reader.read())
                    {
                        sb.append((char)c);
                    }
                    StringTokenizer st = new StringTokenizer(sb.toString(),":");
                    System.out.println("[debug]Otrzymano: " + sb.toString());
                    switch(st.nextToken())
                    {
                        case "Fala":
                            int number = Integer.parseInt(st.nextToken());
                            System.out.println("number = " + number);
                            //System.out.println("childNodes.size() = " + childNodes.size());
                            sendMessage("msgFrom:" + nodeNumber + ":" + (number + 1), 2000);
                            break;
                        default:
                            System.out.println("Otrzymalem niezana wiadomosc!");
                            break;
                    }
                }
            }
        }
        catch (IOException ex) 
        {
           System.err.println(ex);
        } 
    }
    
    public void initialize()
    {
        sendMessage("msgFrom:" + nodeNumber + ":1", 2000);
        server();
    }
    
    private void sendMessage(String msg, int port)
    {
        try(Socket socket = new Socket("localhost", port);) 
        {
            Writer out = new OutputStreamWriter(socket.getOutputStream());
            out.write(msg);
            System.out.println("[debug]Wysłano [sendMessage()] : " + msg);  
            out.flush();
            socket.close();
        } 
        catch (IOException ex) 
        {
            System.err.println(ex);
        }
    }
}
