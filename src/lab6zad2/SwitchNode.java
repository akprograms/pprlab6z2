/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6zad2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author Stud
 */
public class SwitchNode 
{
    private List<List<Integer>> topology;
    private int myPort;
    
    public SwitchNode(List<List<Integer>> topology, int myPort)
    {
        this.topology = topology;
        this.myPort = myPort;
    }
    
    public void server()
    {
        try(ServerSocket server = new ServerSocket(myPort);)
        {
            System.out.println("uruchomiono serwer");
            while(true)
            {
                try(Socket client = server.accept();
                    InputStream in = client.getInputStream();
                    InputStreamReader reader = new InputStreamReader(in,"ASCII");)
                {
                    System.out.println("Klient polaczony");
                    StringBuilder sb = new StringBuilder();
                    for (int c = reader.read(); c != -1;c=reader.read())
                    {
                        sb.append((char)c);
                    }
                    StringTokenizer st = new StringTokenizer(sb.toString(),":");
                    System.out.println("[debug]Otrzymano: " + sb.toString());
                    switch(st.nextToken())
                    {
                        case "msgFrom":
                            int nodeNumber = Integer.parseInt(st.nextToken());
                            int number = Integer.parseInt(st.nextToken());
                            if(topology != null)
                            {
                                for(int n : topology.get(nodeNumber-1))
                                {
                                    sendMessage("Fala:"+ number, n);
                                }
                            }
                            break;
                        default:
                            System.out.println("Otrzymalem niezana wiadomosc!");
                            break;
                    }
                }
            }
        }
        catch (IOException ex) 
        {
           System.err.println(ex);
        } 
    }
    
    private void sendMessage(String msg, int port)
    {
        try(Socket socket = new Socket("localhost", port);) 
        {
            Writer out = new OutputStreamWriter(socket.getOutputStream());
            out.write(msg);
            System.out.println("[debug]Wysłano [sendMessage()] : " + msg);  
            out.flush();
            socket.close();
        } 
        catch (IOException ex) 
        {
            System.err.println(ex);
        }
    }
}
